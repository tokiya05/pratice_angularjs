(function  () {
	// body...
	'use strict'
	var app  = angular.module('GlobalModule', ['ui.router', 'ui.grid']);

	function configFunction ($stateProvider, $urlRouterProvider) {
		// body...
		$urlRouterProvider.otherwise('/home');
		$stateProvider
			.state('home', {
				url: '/home',
				templateUrl: '../app/view/home/index.html'
			})
			.state('home.list', {
				url: '/list',
				templateUrl: '../app/view/home/_list.html',
				controller: function  ($scope) {
					// body...
					$scope.items = ['ABC', 'DEF'];
				}
			})
			.state('home.paragraph', {
				url: '/paragraph',
				template: 'This is an paragraph'
			})
			.state('about', {
				url: '/about',
				views: {
					//main template 
					'': { templateUrl: '../app/view/about/index.html'},
					'leftColumn@about': { template: 'Here is the left column'},
					'rightColumn@about' : {
						templateUrl: '../app/view/about/_table_data.html',
						controller: 'aboutController'
					}	
				}

			})
			.state('user', {
				url :'/user',
				views : {
					'' : { templateUrl: '../app/view/user/index.html'},
					'createUser@user' : {
						templateUrl: '../app/view/user/_create.html'
					},
					'viewUser@user' : {
						templateUrl: '../app/view/user/_table_data.html'
					}
				},
				controller: 'userController'
			});
	}
	configFunction.$inject = ['$stateProvider', '$urlRouterProvider'];
	app.config(configFunction);

})();

