
angular.module("GlobalModule").controller('userController', ['$scope', function($scope){
	$scope.gridOptions = {};
	var colDef = [
		{
			displayName: 'Full Name',
			field: 'name'
		},
		{
			name: 'Age',
			field: 'age'
		},
		{
			name: 'Email Address',
			field: 'email'
		},
		{
			displayName:'Delete',
			cellTemplate: '<button class="btn primary" ng-click ="grid.appScope.Delete(row)">Delete</button>'
		}
	]

	$scope.gridOptions.conlumnDefs = colDef;
    $scope.gridOptions.data = [
			{
				name: "Minh Quang",
				age: 40,
				email: 'minhquang@gmail.com'
 			},
 			{
				name: "Tri Tin",
				age: 32,
				email: 'minhquang@gmail.com'
 			},
 			{
				name: "Nhat Minh",
				age: 2,
				email: 'minhquang@gmail.com'
 			}
	];

	$scope.addUser = function () {
		// body...
	};

	$scope.updateUser = function  () {
		// body...
	}

	$scope.deleteUser = function () {
		
	}
}])