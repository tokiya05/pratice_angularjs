angular.module('Globalmodule.contacts.service', [])
.service('contactsService', ['$http', 'utilsService', function($http, utilsService){
	var path = 'http://192.168.33.11/frontend/app/assets/contacts.json';
	var contacts = $http.get(path).then(function (response) {
		// body...
		return response.data.contacts;
	});

	this.getAll = function  () {
		// body...
		return contacts;
	}

	this.getById = function  (id) {
		// body...
		return contacts.then(function(){
	      return utils.findById(contacts, id);
	    })
	}
}]);