	angular.module('myApp', []).controller('CartController', ['$scope', '$log', function($scope, $log){
		$scope.items = [
			{ title: 'paint pots', quantity: 8, price: 4.2 },
			{ title: 'shoes', quantity: 10, price: 2.3 },
			{ title: 'Pebab', quantity: 20, price: 30.2 }
		];
		$scope.$log = $log;
		$scope.remove = function (index) {
			// body...
			$scope.items.splice(index, 1);
		}
		$scope.addItem = function () {
			// body...
			
			$scope.items.push({
				title: $scope.itemTitle,
				price: $scope.itemPrice,
				quantity: 0
			})
		}
	}]);
	