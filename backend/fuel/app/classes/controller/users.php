<?php
class Controller_Users extends Controller
{

	public function action_index()
	{
		//$data['Users'] = Model_User::find('all');
		$this->template->title = "Users";
		$this->template->content = View::forge('users/index', $data);

	}

	public function action_view($id = null)
	{
		is_null($id) and Response::redirect('users');

		if ( ! $data['User'] = Model_User::find($id))
		{
			Session::set_flash('error', 'Could not find User #'.$id);
			Response::redirect('users');
		}

		$this->template->title = "User";
		$this->template->content = View::forge('users/view', $data);

	}

	public function action_create()
	{
		if (Input::method() == 'POST')
		{
			$val = Model_User::validate('create');

			if ($val->run())
			{
				$User = Model_User::forge(array(
				));

				if ($User and $User->save())
				{
					Session::set_flash('success', 'Added User #'.$User->id.'.');

					Response::redirect('users');
				}

				else
				{
					Session::set_flash('error', 'Could not save User.');
				}
			}
			else
			{
				Session::set_flash('error', $val->error());
			}
		}

		$this->template->title = "Users";
		$this->template->content = View::forge('users/create');

	}

	public function action_edit($id = null)
	{
		is_null($id) and Response::redirect('users');

		if ( ! $User = Model_User::find($id))
		{
			Session::set_flash('error', 'Could not find User #'.$id);
			Response::redirect('users');
		}

		$val = Model_User::validate('edit');

		if ($val->run())
		{

			if ($User->save())
			{
				Session::set_flash('success', 'Updated User #' . $id);

				Response::redirect('users');
			}

			else
			{
				Session::set_flash('error', 'Could not update User #' . $id);
			}
		}

		else
		{
			if (Input::method() == 'POST')
			{

				Session::set_flash('error', $val->error());
			}

			$this->template->set_global('User', $User, false);
		}

		$this->template->title = "Users";
		$this->template->content = View::forge('users/edit');

	}

	public function action_delete($id = null)
	{
		is_null($id) and Response::redirect('users');

		if ($User = Model_User::find($id))
		{
			$User->delete();

			Session::set_flash('success', 'Deleted User #'.$id);
		}

		else
		{
			Session::set_flash('error', 'Could not delete User #'.$id);
		}

		Response::redirect('users');

	}

}
